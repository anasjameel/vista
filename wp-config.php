<?php
/** 
 * Configuración básica de WordPress.
 *
 * Este archivo contiene las siguientes configuraciones: ajustes de MySQL, prefijo de tablas,
 * claves secretas, idioma de WordPress y ABSPATH. Para obtener más información,
 * visita la página del Codex{@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} . Los ajustes de MySQL te los proporcionará tu proveedor de alojamiento web.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** Ajustes de MySQL. Solicita estos datos a tu proveedor de alojamiento web. ** //
/** El nombre de tu base de datos de WordPress */
define('DB_NAME', 'vistadb');

/** Tu nombre de usuario de MySQL */
define('DB_USER', 'root');

/** Tu contraseña de MySQL */
define('DB_PASSWORD', '');

/** Host de MySQL (es muy probable que no necesites cambiarlo) */
define('DB_HOST', 'localhost');

/** Codificación de caracteres para la base de datos. */
define('DB_CHARSET', 'utf8');

/** Cotejamiento de la base de datos. No lo modifiques si tienes dudas. */
define('DB_COLLATE', '');

/**#@+
 * Claves únicas de autentificación.
 *
 * Define cada clave secreta con una frase aleatoria distinta.
 * Puedes generarlas usando el {@link https://api.wordpress.org/secret-key/1.1/salt/ servicio de claves secretas de WordPress}
 * Puedes cambiar las claves en cualquier momento para invalidar todas las cookies existentes. Esto forzará a todos los usuarios a volver a hacer login.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', '[o][JAr[fGP>Kv?&pg~Kt09ZRo%1|}8ZCvoOd0T@.ov[EPa2u-bCP@C6ympy+|.R'); // Cambia esto por tu frase aleatoria.
define('SECURE_AUTH_KEY', '!{:YyJ4Og4P3*/4!_1VL1+f5A1ZDzWvw7^E=E7P)D]J6|>Op4WY&, bZ#D}}%xMf'); // Cambia esto por tu frase aleatoria.
define('LOGGED_IN_KEY', 'd}5Q80->Q@WCI.ML-FUk=zv?^lm)`{paZ0+maCS7RM.7bbkm2mw`3cA^%E!-AdYt'); // Cambia esto por tu frase aleatoria.
define('NONCE_KEY', '@<l8P2$n-:*-&1|1CUc2GOF|d{|Sj3;~Wn2qib9MSr|PW|po.f,ak*/l_h_s2x94'); // Cambia esto por tu frase aleatoria.
define('AUTH_SALT', '!*r^-NG-M[;k; -Qo|PJ_s:=i~+#OP? l2y;MuTW#G{cZB?xs%sjeo+_z-Rx[z^U'); // Cambia esto por tu frase aleatoria.
define('SECURE_AUTH_SALT', 'gVe+23N^h$yE+c|>`LLmj?h!/Nn<Ps-A0Of9Z_gHbZP2fh!.hLsX:AmKD|iq68:X'); // Cambia esto por tu frase aleatoria.
define('LOGGED_IN_SALT', '_TSB|t9(h6A]U6lTA6]}7j9K0yda>B)#{/P)=lh.+Df5h4=6_Rk{akhru8l<TB|_'); // Cambia esto por tu frase aleatoria.
define('NONCE_SALT', '$zg=&?8/rt)]:k(w(&tqsEU^RLo-xx<S:zoFRRhNkA+^V4t6/G!!fGDed;si+I%j'); // Cambia esto por tu frase aleatoria.

/**#@-*/

/**
 * Prefijo de la base de datos de WordPress.
 *
 * Cambia el prefijo si deseas instalar multiples blogs en una sola base de datos.
 * Emplea solo números, letras y guión bajo.
 */
$table_prefix  = 'wp_';

/**
 * Idioma de WordPress.
 *
 * Cambia lo siguiente para tener WordPress en tu idioma. El correspondiente archivo MO
 * del lenguaje elegido debe encontrarse en wp-content/languages.
 * Por ejemplo, instala ca_ES.mo copiándolo a wp-content/languages y define WPLANG como 'ca_ES'
 * para traducir WordPress al catalán.
 */
define('WPLANG', 'es_ES');

/**
 * Para desarrolladores: modo debug de WordPress.
 *
 * Cambia esto a true para activar la muestra de avisos durante el desarrollo.
 * Se recomienda encarecidamente a los desarrolladores de temas y plugins que usen WP_DEBUG
 * en sus entornos de desarrollo.
 */
define('WP_DEBUG', false);

/* ¡Eso es todo, deja de editar! Feliz blogging */

/** WordPress absolute path to the Wordpress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

